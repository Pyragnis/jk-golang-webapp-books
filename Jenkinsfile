pipeline {
    agent any

    environment {
        DOCKER_BIN = 'docker'
        DOCKER_IMAGE = 'pyragnis91/my-golang-app'
        IMAGE_TAG = 'latest'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login'
        HEROKU_APP_NAME = 'golang2'
    }

    stages {
        stage('Build image') {
            steps {
                script {
                    bat "${DOCKER_BIN} build -t ${DOCKER_IMAGE}:${IMAGE_TAG} ."
                }
            }
        }

        stage('Push image to Docker Hub') {
            steps {
                script {
                    // Pushing the Docker image to Docker Hub
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'DOCKERHUB_USER', passwordVariable: 'DOCKERHUB_PASS')]) {
                        docker.withRegistry('https://docker.io', 'dockerhub-login') {
                            def dockerImage = docker.image("${DOCKER_IMAGE}:${IMAGE_TAG}")
                            dockerImage.push()
                        }
                    }
                }
            }
        }

        stage('Deploy to Heroku') {
            steps {
                script {
                    // Deploying the Docker image to Heroku
                    withCredentials([string(credentialsId: 'heroku_api_key', variable: 'HEROKU_API_KEY')]) {
                        bat """
                        ${DOCKER_BIN} tag ${DOCKER_IMAGE}:${IMAGE_TAG} registry.heroku.com/${HEROKU_APP_NAME}/web
                        echo $HEROKU_API_KEY | ${DOCKER_BIN} login --username=_ --password-stdin registry.heroku.com
                        ${DOCKER_BIN} push registry.heroku.com/${HEROKU_APP_NAME}/web
                        heroku container:release web --app ${HEROKU_APP_NAME}
                        """
                    }
                }
            }
        }

        stage('Check Deployment') {
            steps {
                script {
                    bat "curl -s https://${HEROKU_APP_NAME}.herokuapp.com"
                }
            }
        }
    }

    post {
        always {
           
            
            emailext(
                to: 'notif-jenkins@joelkoussawo.me',
                subject: "Build ${currentBuild.fullDisplayName} completed",
                body: """
                <html><body>
                <p>La construction de <strong>${env.JOB_NAME}</strong> numéro <strong>${env.BUILD_NUMBER}</strong> est terminée.</p>
                <p>Statut : <strong>${currentBuild.currentResult}</strong></p>
                <p>Voir les détails de la construction à <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                </body></html>""",
                mimeType: 'text/html'
            )
        }
        success {
            echo 'La construction et le déploiement ont réussi !'
        }
        failure {
            echo 'La construction ou le déploiement ont échoué.'
        }
    }
}
