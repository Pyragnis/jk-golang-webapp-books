FROM golang:1.19.2-alpine AS builder

# Set working directory for build context
WORKDIR /app

# Copy Go source code from current directory
COPY . .

# Install dependencies
RUN go mod download

# Build the Go binary as 'jk-golang-webapp-books'
RUN go build -o jk-golang-webapp-books main.go

# Use a slimmer image for the final artifact
FROM alpine:3.16

# Add ca-certificates in case your Go application makes outbound HTTPS requests
RUN apk --no-cache add ca-certificates

# Set working directory for running application
WORKDIR /root/

# Copy the built binary from the builder stage
COPY --from=builder /app/jk-golang-webapp-books .

# Set the entrypoint to run the binary named 'jk-golang-webapp-books'
ENTRYPOINT ["./jk-golang-webapp-books"]

# Expose a default port, can be overridden by runtime environment
EXPOSE 8081
